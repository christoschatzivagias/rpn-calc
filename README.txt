Reverse Polish Notation Calculator written in C++ with lex for lexical analysis and bison for syntax analysis
mkdir build
cd build
cmake ../ -DCMAKE_INSTALL_PREFIX=../install_dir
make
make test
make install

or run the build.sh 

Requirements
gcc 7.5
CMake 3.3 and above
Flex 2.6.4
Bison 3.0.4

