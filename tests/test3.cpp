#include "operations.h"
#include "test.h"

int main(int , char **)
{
    std::string input = "27 pi * 20 / round";
    yy_scan_string(input.c_str());
    yyparse();
    ALEPH_ASSERT_EQUAL(4, get_stack().at(0).second.ToInt());
    return EXIT_SUCCESS;
}
