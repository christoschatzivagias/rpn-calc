#include "operations.h"
#include "test.h"
#include <cstring>

int main(int , char **)
{
    std::string input = "4";
    yy_scan_string(input.c_str());
    yyparse();
    input = "macro kib 1024 *";
    yy_scan_string(input.c_str());
    yyparse();
    input = "macro mib 1024 1024 * *";
    yy_scan_string(input.c_str());
    yyparse();
    input = "mib x= x x + 1 mib /";
    yy_scan_string(input.c_str());
    yyparse();
    ALEPH_ASSERT_EQUAL(std::strcmp(get_stack().at(0).first.c_str(),"x"), 0);
    ALEPH_ASSERT_EQUAL(4194304, get_stack().at(0).second.ToInt());
    ALEPH_ASSERT_EQUAL(8, get_stack().at(1).second.ToInt());
    return EXIT_SUCCESS;
}
