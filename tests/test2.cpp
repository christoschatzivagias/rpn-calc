#include "operations.h"
#include "test.h"


int main(int , char **)
{
    std::string input = "9 3 repeat dup * * swap drop sqrt ";
    yy_scan_string(input.c_str());
    yyparse();
    ALEPH_ASSERT_EQUAL(27, get_stack().at(0).second.ToInt());
    return EXIT_SUCCESS;
}
