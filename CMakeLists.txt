cmake_minimum_required (VERSION 3.3)
project(rpn-calc)

set(CMAKE_CXX_STANDARD 14)
enable_language(CXX)
ENABLE_TESTING()


find_package(FLEX REQUIRED)
find_package(BISON REQUIRED)

flex_target(lexer src/rpnlexer.l "${CMAKE_CURRENT_BINARY_DIR}/rpnlexer.cpp")

bison_target(parser src/rpnparser.y "${CMAKE_CURRENT_BINARY_DIR}/rpnparser.cpp")

include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(${CMAKE_CURRENT_SOURCE_DIR})
include_directories(ttmath)
include_directories(include)

set(PARSE_FILES
    src/rpnlexer.l
    src/rpnparser.y
    )

set(RPN_HEADER_FILES
	include/operations.h
        include/test.h
 )

set(RPN_SRC_FILES
	src/operations.cpp
        "${CMAKE_CURRENT_BINARY_DIR}/rpnlexer.cpp"
        "${CMAKE_CURRENT_BINARY_DIR}/rpnparser.cpp"
 )
add_library(rpnlib STATIC ${RPN_HEADER_FILES} ${RPN_SRC_FILES})

add_executable(rpn src/main.cpp)
target_link_libraries(rpn rpnlib)
add_executable(rpn_test tests/test.cpp)
add_executable(rpn_test2 tests/test2.cpp)
add_executable(rpn_test3 tests/test3.cpp)
add_executable(rpn_test4 tests/test4.cpp)

target_link_libraries(rpn_test rpnlib)
target_link_libraries(rpn_test2 rpnlib)
target_link_libraries(rpn_test3 rpnlib)
target_link_libraries(rpn_test4 rpnlib)

add_test(rpn rpn_test)
add_test(rpn2 rpn_test2)
add_test(rpn3 rpn_test3)
add_test(rpn4 rpn_test4)

install(TARGETS rpn DESTINATION ${CMAKE_INSTALL_PREFIX})

