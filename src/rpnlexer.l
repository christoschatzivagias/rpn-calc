%option noyywrap
%option nounput

%{
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "rpnparser.hpp"

%}

%%
[0]|[1-9][0-9]*            {
                              yylval.number = atof(yytext);
                              return NUMBER;
                           }
[0][.][0-9]*               {
                              yylval.number = atof(yytext);
                              return NUMBER;
                           }
[1-9][0-9]*[.][0-9]*       {
                              yylval.number = atof(yytext);
                              return NUMBER;
                           }
"pi"                       {
                              yylval.number = M_PI;
                              return NUMBER;
                           }
"e"                        {
                              yylval.number = std::exp(1.0);
                              return NUMBER;
                           }
"rand"                     {
                              yylval.number = rand()%1000;
                              return NUMBER;
                           }
"+"                        {
                              yylval.integer = ADD;
                              return ADD;
                           }
"-"                        {
                              yylval.integer = SUB;
                              return SUB;
                           }
"*"                        {
                              yylval.integer = MULT;
                              return MULT;
                           }
"/"                        {
                              yylval.integer = DIV;
                              return DIV;
                           }
"clr"                      {
                              yylval.integer = CLVARS;
                              return CLVARS;
                           }
"clv"                      {
                              yylval.integer = CLVALS;
                              return CLVALS;
                           }
"cla"                      {
                              yylval.integer = CL;
                              return CL;
                           }
"!"                        {
                              yylval.integer = BNOT;
                              return BNOT;
                           }
"!="                       {
                              yylval.integer = NOTEQ;
                              return NOTEQ;
                           }
"%"                        {
                              yylval.integer = MOD;
                              return MOD;
                           }
"++"                       {
                              yylval.integer = INC;
                              return INC;
                           }
"--"                       {
                              yylval.integer = DEC;
                              return DEC;
                           }
"="                        {
                              yylval.integer = VARSET;
                              return VARSET;
                           }
"&"                        {
                              yylval.integer = BWAND;
                              return BWAND;
                           }
"|"                        {
                              yylval.integer = BWOR;
                              return BWOR;
                           }
"^"                        {
                              yylval.integer = BWXOR;
                              return BWXOR;
                           }
"~"                        {
                              yylval.integer = BWNOT;
                              return BWNOT;
                           }
"<<"                       {
                              yylval.integer = BWSFTL;
                              return BWSFTL;
                           }
">>"                       {
                              yylval.integer = BWSFTR;
                              return BWSFTR;
                           }
"&&"                       {
                              yylval.integer = BAND;
                              return BAND;
                           }
"||"                       {
                              yylval.integer = BOR;
                              return BOR;
                           }
"^^"                       {
                              yylval.integer = BXOR;
                              return BXOR;
                           }
"<"                        {
                              yylval.integer = LESS;
                              return LESS;
                           }
"<="                       {
                              yylval.integer = LESS_EQ;
                              return LESS_EQ;
                           }
"=="                       {
                              yylval.integer = EQ;
                              return EQ;
                           }
">"                        {
                              yylval.integer = GREATER;
                              return GREATER;
                           }
">="                       {
                              yylval.integer = GREATER_EQ;
                              return GREATER_EQ;
                           }
"acos"                     {
                              yylval.integer = ACOS;
                              return ACOS;
                           }
"asin"                     {
                              yylval.integer = ASIN;
                              return ASIN;
                           }
"atan"                     {
                              yylval.integer = ATAN;
                              return ATAN;
                           }
"cos"                      {
                              yylval.integer = COS;
                              return COS;
                           }
"cosh"                     {
                              yylval.integer = COSH;
                              return COSH;
                           }
"sin"                      {
                              yylval.integer = SIN;
                              return SIN;
                           }
"sinh"                     {
                              yylval.integer = SINH;
                              return SINH;
                           }
"tanh"                     {
                              yylval.integer = TANH;
                              return TANH;
                           }
"ceil"                     {
                              yylval.integer = CEIL;
                              return CEIL;
                           }
"floor"                    {
                              yylval.integer = FLOOR;
                              return FLOOR;
                           }
"round"                    {
                              yylval.integer = ROUND;
                              return ROUND;
                           }
"ip"                       {
                              yylval.integer = IP;
                              return IP;
                           }
"fp"                       {
                              yylval.integer = FP;
                              return FP;
                           }
"sign"                     {
                              yylval.integer = SIGN;
                              return SIGN;
                           }
"abs"                      {
                              yylval.integer = ABS;
                              return ABS;
                           }
"max"                      {
                              yylval.integer = MAX;
                              return MAX;
                           }
"min"                      {
                              yylval.integer = MIN;
                              return MIN;
                           }
"exp"                      {
                              yylval.integer = EXP;
                              return EXP;
                           }
"fact"                     {
                              yylval.integer = FACT;
                              return FACT;
                           }
"sqrt"                     {
                              yylval.integer = SQRT;
                              return SQRT;
                           }
"ln"                       {
                              yylval.integer = LN;
                              return LN;
                           }
"log"                      {
                              yylval.integer = LOG;
                              return LOG;
                           }
"pow"                      {
                              yylval.integer = POW;
                              return POW;
                           }
"hnl"                      {
                              yylval.integer = HNL;
                              return HNL;
                           }
"hns"                      {
                              yylval.integer = HNS;
                              return HNS;
                           }
"nhl"                      {
                              yylval.integer = NHL;
                              return NHL;
                           }
"nhs"                      {
                              yylval.integer = NHS;
                              return NHS;
                           }
"pick"                     {
                              yylval.integer = PICK;
                              return PICK;
                           }
"repeat"                   {
                              yylval.integer = REPEAT;
                              return REPEAT;
                           }
"depth"                    {
                              yylval.integer = DEPTH;
                              return DEPTH;
                           }
"drop"                     {
                              yylval.integer = DROP;
                              return DROP;
                           }
"dropn"                    {
                              yylval.integer = DROPN;
                              return DROPN;
                           }
"dup"                      {
                              yylval.integer = DUP;
                              return DUP;
                           }
"dupn"                     {
                              yylval.integer = DUPN;
                              return DUPN;
                           }
"roll"                     {
                              yylval.integer = ROLL;

                              return ROLL;
                           }
"rolld"                    {
                              yylval.integer = ROLLD;
                              return ROLLD;
                           }
"stack"                    {
                              yylval.integer = STACK;
                              return STACK;
                           }
"hex"                      {
                              yylval.integer = HEX;
                              return HEX;
                           }
"dec"                      {
                              yylval.integer = DECI;
                              return DECI;
                           }
"bin"                      {
                              yylval.integer = BIN;
                              return BIN;
                           }
"oct"                      {
                              yylval.integer = OCT;
                              return OCT;
                           }
"swap"                     {
                              yylval.integer = SWAP;
                              return SWAP;
                           }
"macro"                    {
                              yylval.integer = MACRO;
                              return MACRO;
                           }
"help"                     {
                              return HELP;
                           }
"exit"                     {
                              return EXIT;
                           }
[ \t\r]+                   {
                           }
"\n"                       {

                           }
[a-zA-Z]([a-zA-Z]|[0-9])*  {
                                yylval.string = strdup(yytext);
                                return STR;
                           }
. return yytext[0];


%%
