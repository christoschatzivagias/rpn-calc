#include <string>
#include <map>
#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <netinet/in.h>
#include <bitset>
#include <climits>
#include <sstream>
#include "operations.h"

std::map<std::string, std::vector<macro_op>> macros;
std::vector<t_var> stack;
enum StackPrintDisplayMode stack_print_display_mode = DECIMAL;
enum StackPrintLayoutType stack_print_layout = Horizontal;
std::vector<macro_op> macros_buffer;


std::vector<macro_op> &get_macros_buffer()
{
    return macros_buffer;
}

void add_macro_operation(enum OperationType otype, const std::string &num)
{
    macros_buffer.push_back(macro_op(otype,num));
}

void clear_macros_buffer()
{
    macros_buffer.clear();
}

void handle_command_status(int command_status)
{
    if(command_status == ERROR){
       yyerror("unknown error");
    } else if(command_status == NOT_ENOUGH_ARGS) {
        yyerror("not enough args from stack");
    } else if(command_status ==  INVALID_ARG) {
        yyerror("invalid args");
    }
}

void add_macro(const std::string &name)
{
    t_var t = get_variable(get_stack(), name);
    if(t.first == ""){
        macros[name] = macros_buffer;
    } else {
        std::string error = "can not declare macro with name " + name + ",already defined as variable\n";
         yyerror(error.c_str());
    }
    macros_buffer.clear();
}

std::vector<macro_op> get_macro(const std::string &name)
{
    std::map<std::string, std::vector<macro_op>>::const_iterator it = macros.find(name);
    if(it == macros.cend())
        return std::vector<macro_op>();

    return it->second;
}

std::vector<t_var> &get_stack()
{
    return stack;
}

enum StackPrintDisplayMode get_stack_display_mode()
{
    return stack_print_display_mode;
}

void set_stack_print_display_mode(enum StackPrintDisplayMode value)
{
    stack_print_display_mode = value;
}

enum StackPrintLayoutType get_stack_print_layout()
{
    return stack_print_layout;
}

void set_stack_print_layout(enum StackPrintLayoutType value)
{
    stack_print_layout = value;
}

void print_help()
{
    Color::Modifier red(Color::FG_RED);
    Color::Modifier green(Color::FG_GREEN);
    Color::Modifier blue(Color::FG_BLUE);
    Color::Modifier def(Color::FG_DEFAULT);
    std::cout  << "--Reverse Polish Notation interpreter--" << std::endl
               << "Use examples: " << std::endl
               << std::endl
               << blue << " > 2 2 3" << std::endl
               << " 2 2 3  > + *" << std::endl
               << " 10  > dup" << std::endl
               << " 10 10  > pi *" << std::endl
               << " 10 31.41592653589793115997963468544185161590576171875  > 10 / " << std::endl
               << " 10 3.141592653589793115997963468544185161590576171875  > cos round" << std::endl
               << " 10 -1  > " << std::endl
               << " 10 -1  > 3 *" << std::endl
               << " 10 -3  > 3 repeat dup" << std::endl
               << " 10 -3 -3 -3 -3  > 3 repeat *" << std::endl
               << " 10 81  > -1 *" << std::endl
               << " 10 -81  > hex" << std::endl
               << " A -51  > bin" << std::endl
               << " 1010 -1010001  > oct" << std::endl
               << " 12 -121  > " << std::endl << def << std::endl



               << "Error definition:" << std::endl
               << "\t" << red << "syntax error" << def << ": parsing of input failed invalid syntax" << std::endl
               << "\t" << red << "unknown error" << def << ": unknown error" << std::endl
               << "\t" << red << "not enough args from stack" << def << ": The stack is propably empty or has only one value and the operation could not be executed" << std::endl
               << "\t" << red << "invalid args" << def << ": The args of the operation are invalid i.e. negative number in pick function" << std::endl
               << "\t" << red << "can not declare macro with name ..., already defined as variable" << def << ": The variable is already set" << std::endl
               << "\t" << red << "unknown variable or macro" << def << ": The macro or variable you are trying to access is not defined" << std::endl
               << std::endl
               << "Command Set Help" << std::endl
	       << std::endl
               << "Arithmetic Operators" << std::endl

               << green << "\t+          " << def << "Add" << std::endl
               << green << "\t-          " << def << "Subtract" << std::endl
               << green << "\t*          " << def << "Multiply" << std::endl
               << green << "\t/          " << def << "Divide" << std::endl
               << green << "\tcla        " << def << "Clear the stack and variables" << std::endl
               << green << "\tclr        " << def << "Clear the stack" << std::endl
               << green << "\tclv        " << def << "Clear the variables" << std::endl
               << green << "\t!          " << def << "Boolean NOT" << std::endl
               << green << "\t!=         " << def << "Not equal to" << std::endl
               << green << "\t%          " << def << "Modulus" << std::endl
               << green << "\t++         " << def << "Increment" << std::endl
               << green << "\t--         " << def << "Decrement" << std::endl

	       << std::endl
               << "Bitwise Operators" << std::endl

               << green << "\t&          " << def << "Bitwise AND" << std::endl
               << green << "\t|          " << def << "Bitwise OR" << std::endl
               << green << "\t^          " << def << "Bitwise XOR" << std::endl
               << green << "\t~          " << def << "Bitwise NOT" << std::endl
               << green << "\t<<         " << def << "Bitwise shift left" << std::endl
               << green << "\t>>         " << def << "Bitwise shift right" << std::endl

	       << std::endl
               << "Boolean Operators" << std::endl

               << green << "\t&&         " << def << "Boolean AND" << std::endl
               << green << "\t||         " << def << "Boolean OR" << std::endl
               << green << "\t^^         " << def << "Boolean XOR" << std::endl

	       << std::endl
               << "Comparison Operators" << std::endl

               << green << "\t<          " << def << "Less than" << std::endl
               << green << "\t<=         " << def << "Less than or equal to" << std::endl
               << green << "\t==         " << def << "Equal to" << std::endl
               << green << "\t>          " << def << "Greater than" << std::endl
               << green << "\t>=         " << def << "Greater than or equal to" << std::endl

	       << std::endl
               << "Trigonometric Functions" << std::endl

               << green << "\tacos       " << def << "Arc Cosine" << std::endl
               << green << "\tasin       " << def << "Arc Sine" << std::endl
               << green << "\tatan       " << def << "Arc Tangent" << std::endl
               << green << "\tcos        " << def << "Cosine" << std::endl
               << green << "\tcosh       " << def << "Hyperbolic Cosine" << std::endl
               << green << "\tsin        " << def << "Sine" << std::endl
               << green << "\tsinh       " << def << "Hyperbolic Sine" << std::endl
               << green << "\ttanh       " << def << "Hyperbolic tangent" << std::endl

	       << std::endl
               << "Numeric Utilities" << std::endl

               << green << "\tceil       " << def << "Ceiling" << std::endl
               << green << "\tfloor      " << def << "Floor" << std::endl
               << green << "\tround      " << def << "Round" << std::endl
               << green << "\tip         " << def << "Integer part" << std::endl
               << green << "\tfp         " << def << "Floating part" << std::endl
               << green << "\tsign       " << def << "Push -1, 0, or 0 depending on the sign" << std::endl
               << green << "\tabs        " << def << "Absolute value" << std::endl
               << green << "\tmax        " << def << "Max" << std::endl
               << green << "\tmin        " << def << "Min" << std::endl

	       << std::endl
               << "Display Modes" << std::endl

               << green << "\thex        " << def << "Switch display mode to hexadecimal" << std::endl
               << green << "\tdec        " << def << "Switch display mode to decimal (default)" << std::endl
               << green << "\tbin        " << def << "Switch display mode to binary" << std::endl
               << green << "\toct        " << def << "Switch display mode to octal" << std::endl

	       << std::endl
               << "Constants" << std::endl

               << green << "\te          " << def << "Push e" << std::endl
               << green << "\tpi         " << def << "Push Pi" << std::endl
               << green << "\trand       " << def << "Generate a random number" << std::endl

	       << std::endl
               << "Mathematic Functions" << std::endl

               << green << "\texp        " << def << "Exponentiation" << std::endl
               << green << "\tfact       " << def << "Factorial" << std::endl
               << green << "\tsqrt       " << def << "Square Root" << std::endl
               << green << "\tln         " << def << "Natural Logarithm" << std::endl
               << green << "\tlog        " << def << "Logarithm" << std::endl
               << green << "\tpow        " << def << "Raise a number to a power" << std::endl

	       << std::endl
               << "Networking" << std::endl

               << green << "\thnl        " << def << "Host to network long" << std::endl
               << green << "\thns        " << def << "Host to network short" << std::endl
               << green << "\tnhl        " << def << "Network to host long" << std::endl
               << green << "\tnhs        " << def << "Network to host short" << std::endl

	       << std::endl
               << "Stack Manipulation" << std::endl

               << green << "\tpick       " << def << "Pick the -n'th item from the stack" << std::endl
               << green << "\trepeat     " << def << "Repeat an operation n times, e.g. '3 repeat +'" << std::endl
               << green << "\tdepth      " << def << "Push the current stack depth" << std::endl
               << green << "\tdrop       " << def << "Drops the top item from the stack" << std::endl
               << green << "\tdropn      " << def << "Drops n items from the stack" << std::endl
               << green << "\tdup        " << def << "Duplicates the top stack item" << std::endl
               << green << "\tdupn       " << def << "Duplicates the top n stack items in order" << std::endl
               << green << "\troll       " << def << "Roll the stack upwards by n" << std::endl
               << green << "\trolld      " << def << "Roll the stack downwards by n" << std::endl
               << green << "\tstack      " << def << "Toggles stack display from horizontal to vertical" << std::endl
               << green << "\tswap       " << def << "Swap the top 2 stack items" << std::endl

	       << std::endl
               << "Macros and Variables" << std::endl

               << green << "\tmacro      " << def << "Defines a macro, e.g. 'macro kib 1024 *'" << std::endl
               << green << "\tx=         " << def << "Assigns a variable, e.g. '1024 x='" << std::endl

	       << std::endl
               << "Other" << std::endl

               << green << "\thelp       " << def << "Print the help message" << std::endl
               << green << "\texit       " << def << "Exit the calculator" << std::endl;
}

void print_hexa(float num)
{
    FloatUnion x;
    x.f = num;
    for (size_t i=0; i<sizeof(float); i++)
        printf( "%X", x.c[i] );
}

void print_octa(float num)
{
    FloatUnion x;
    x.f = num;
    for (size_t i=0; i<sizeof(float); i++)
        printf( "%o", x.c[i] );
}

void print_bin(float num)
{
    FloatUnion x;
    x.f = num;

    std::bitset<sizeof(float) * CHAR_BIT> bits(x.i);
    std::cout << bits;
}

void print_var_format(const t_var &val, int number = 10, char changeCh = ' ')
{
    std::string s;
    val.second.ToString(s, number);
    std::cout << "[" << val.first << "=" << s << "]" << changeCh;
}

void print_num_format(const t_var &val, int number = 10, char changeCh = ' ')
{
    std::string s;
    val.second.ToString(s, number);
    std::cout << s << changeCh;
}

void print_stack(std::vector<t_var> s)
{
    char changeCh = ' ';
    if(get_stack_print_layout() == Vertical)
        changeCh = '\n';
    std::for_each(s.cbegin(), s.cend(), [&](const t_var &val){
        if(val.first != "") {
            print_var_format(val, get_stack_display_mode(), changeCh);
        } else {
            print_num_format(val, get_stack_display_mode(), changeCh);
        }
    });
}

t_var get_variable(const std::vector<t_var> &st, const std::string &name)
{
    std::vector<t_var>::const_iterator it = std::find_if (st.cbegin(), st.cend(), [&](const t_var &v){
        return v.first == name;
    });
    if(it == st.cend()){
        return t_var("",0);
    }
    return *it;
}

bool check_double_integer(double val)
{
    if ((val - (int)val) == 0)
        return true;

    return false;
}

bool replace(std::string& str, const std::string& from, const std::string& to) {
    size_t start_pos = str.find(from);
    if(start_pos == std::string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
}

std::vector<std::string> split(const std::string &s)
{
    std::stringstream ss(s);
    std::string item;
    std::vector<std::string> elems;
    while (std::getline(ss, item, ' ')) {
        if(item != "")
            elems.push_back(item);
    }
    return elems;
}

int number(std::vector<t_var> &st, ttmath::Big<10,20> num)
{
    st.push_back(t_var("",num));
    return OK;
}

int add(std::vector<t_var> &st)
{
	if(st.size() < 2)
		return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val2.first, val2.second + val1.second));
	return OK;
}

int subtract(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val2.first, val2.second - val1.second));
    return OK;
}

int multiply(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val2.first, val2.second * val1.second));
    return OK;
}

int divide(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val2.first, val2.second / val1.second));

    return OK;
}

int clearvals(std::vector<t_var> &st)
{
    st.erase(std::remove_if(st.begin(), st.end(),
                           [](const t_var v) { return v.first!=""; }), st.end());
    return OK;
}

int clearvars(std::vector<t_var> &st)
{
    st.erase(std::remove_if(st.begin(), st.end(),
                           [](const t_var v) { return v.first==""; }), st.end());
    return OK;
}

int clear(std::vector<t_var> &st)
{
    st.clear();
    return OK;
}

int booleannot(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, !(val1.second != 0)));
    return OK;
}

int notequalto(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val2.first, val2.second != val1.second));

    return OK;
}

int modulus(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();

    st.push_back(t_var(val2.first, val2.second.Mod(val1.second)));
    return OK;
}

int increment(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, val1.second + 1));
    return OK;
}

int decrement(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();

    st.push_back(t_var(val1.first, val1.second - 1));
    return OK;
}

int bitwand(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();

    st.push_back(t_var(val2.first, val2.second.BitAnd(val1.second)));
    return OK;
}

int bitwor(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();

    st.push_back(t_var(val2.first, val2.second.BitOr(val1.second)));
    return OK;
}

int bitwxor(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();

    st.push_back(t_var(val2.first, val2.second.BitXor(val1.second)));
    return OK;
}

int bitwnot(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, ~val1.second.ToUInt()));
    return OK;
}

int bitwshiftleft(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val2.first, val2.second.ToUInt() << val1.second.ToUInt()));
    return OK;
}

int bitwshiftright(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val2.first, val2.second.ToUInt() >> val1.second.ToUInt()));
    return OK;
}

int band(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val2.first, (val2.second != 0) && (val1.second != 0)));
    return OK;
}

int bor(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val2.first, (val2.second != 0) || (val1.second != 0)));
    return OK;
}

int bxor(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val2.first, (val2.second != 0) != (val1.second != 0)));
    return OK;
}

int lessthan(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val2.first, val2.second < val1.second));
    return OK;
}

int lessthanorequal(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val2.first, val2.second <= val1.second));

    return OK;
}

int equal(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val2.first, val2.second == val1.second));
    return OK;
}

int greaterthan(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val2.first, val2.second > val1.second));
    return OK;
}

int greaterthanorequal(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val2.first, val2.second >= val1.second));
    return OK;
}

int acos(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, ttmath::ACos(val1.second)));

    return OK;
}

int asin(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, ttmath::ASin(val1.second)));

    return OK;
}

int atan(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, ttmath::ATan(val1.second)));

    return OK;
}

int cos(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, ttmath::Cos(val1.second)));
    return OK;
}

int cosh(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, ttmath::Cosh(val1.second)));
    return OK;
}

int sin(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, ttmath::Sin(val1.second)));
    return OK;
}

int sinh(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, ttmath::Sinh(val1.second)));
    return OK;
}

int tanh(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, ttmath::Tanh(val1.second)));
    return OK;
}

int ceil(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, ttmath::Ceil(val1.second)));
    return OK;
}

int floor(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, ttmath::Floor(val1.second)));

    return OK;
}

int round(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, ttmath::Round(val1.second)));
    return OK;
}

int ip(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, val1.second.ToInt()));

    return OK;
}

int fp(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, val1.second - val1.second.ToInt()));
    return OK;
}

int sign(std::vector<t_var> &)
{
    return OK;
}

int abs(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, ttmath::Abs(val1.second)));
    return OK;
}

int max(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val2.first, std::max(val2.second,val1.second)));

    return OK;
}

int min(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val2.first, std::min(val2.second,val1.second)));

    return OK;
}

int exp(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, ttmath::Exp(val1.second)));

    return OK;
}

int fact(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();

    st.pop_back();
    st.push_back(t_var(val1.first, ttmath::Factorial(val1.second)));
    return OK;
}


int sqrt(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, ttmath::Sqrt(val1.second)));

    return OK;
}

int ln(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, ttmath::Ln(val1.second)));
    return OK;
}

int log(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, ttmath::Log(val2.second,val1.second)));

    return OK;
}

int pow(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, val1.second.Pow(val2.second)));

    return OK;
}


int hnl(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, htonl(val1.second.ToUInt())));

    return OK;
}

int hns(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, htons(val1.second.ToUInt())));

    return OK;
}

int nhl(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, ntohl(val1.second.ToUInt())));

    return OK;
}

int nhs(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();
    st.push_back(t_var(val1.first, ntohs(val1.second.ToUInt())));

    return OK;
}

int pick(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;


    t_var val1 = st.back();
    st.pop_back();

    unsigned int n = val1.second.ToUInt();

    if(n < 1) {
        return INVALID_ARG;
    }
    if(st.size() < n) {
        return INVALID_ARG;
    }

    st.erase (st.begin() + n - 1);
    return OK;
}
int repeat(std::vector<t_var> &st);

int depth(std::vector<t_var> &st)
{
    st.push_back(t_var(std::string(),st.size()));
    return OK;
}

int drop(std::vector<t_var> &st)
{
    st.pop_back();
    return OK;
}

int dropn(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();

    for(unsigned int i = 0; i < val1.second.ToUInt(); i++)
        st.pop_back();
    return OK;
}

int dup(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    st.push_back(st.back());
    return OK;
}

int dupn(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();

    unsigned int n = val1.second.ToUInt();

    if(st.size() < n)
        return ERROR;

    unsigned int s = st.size();
    for(unsigned int i = s - n; i < s; i++)
        st.push_back(st[i]);

    return OK;
}

int rolld(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();

    std::rotate(st.begin(), st.begin() + val1.second.ToUInt(), st.end());
    return OK;
}

int roll(std::vector<t_var> &st)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();

    for(unsigned int index = 0; index < val1.second.ToUInt(); index++) {
        t_var val = st.back();
        st.pop_back();
        st.insert(st.begin(), val);
    }
    return OK;
}

int swap(std::vector<t_var> &st)
{
    if(st.size() < 2)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    st.pop_back();
    t_var val2 = st.back();
    st.pop_back();

    st.push_back(val1);
    st.push_back(val2);

    return OK;
}

int varset(std::vector<t_var> &st, const std::string &varname)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    t_var t = get_variable(get_stack(), varname);
    if(t.first == varname){
	    std::string error = "can not declare macro with name " + varname + " already defined as variable";
	    yyerror(error.c_str());
	    return VARIABLE_EXISTS;
    }
    st.pop_back();

    val1.first = varname;
    st.push_back(val1);

    return OK;
}

int execute_operation(std::vector<t_var> &st, enum yytokentype toktype)
{
    switch(toktype)
    {
          case NUMBER:break;
          case SPACE:break;
          case STR:break;
          case ADD:return add(st);break;
          case SUB:return subtract(st);break;
          case MULT:return multiply(st);break;
          case DIV:return divide(st);break;
          case CLVALS:return clearvals(st);break;
          case CLVARS:return clearvars(st);break;
          case CL:return clear(st);break;
          case BNOT:return booleannot(st);break;
          case NOTEQ:return notequalto(st);break;
          case MOD:return modulus(st);break;
          case INC:return increment(st);break;
          case DEC:return decrement(st);break;
          case BWAND:return bitwand(st);break;
          case BWOR:return bitwor(st);break;
          case BWXOR:return bitwxor(st);break;
          case BWNOT:return bitwnot(st);break;
          case BWSFTL:return bitwshiftleft(st);break;
          case BWSFTR:return bitwshiftright(st);break;
          case BAND:return band(st);break;
          case BOR:return bor(st);break;
          case BXOR:return bxor(st);break;
          case LESS:return lessthan(st);break;
          case LESS_EQ:return lessthanorequal(st);break;
          case EQ:return equal(st);break;
          case GREATER:return greaterthan(st);break;
          case GREATER_EQ:return greaterthanorequal(st);break;
          case ACOS:return acos(st);break;
          case ASIN:return asin(st);break;
          case ATAN:return atan(st);break;
          case COS:return cos(st);break;
          case COSH:return cosh(st);break;
          case SIN:return sin(st);break;
          case SINH:return sinh(st);break;
          case TANH:return tanh(st);break;
          case HEX:break;
          case DECI:break;
          case BIN:break;
          case OCT:break;
          case CEIL:return ceil(st);break;
          case FLOOR:return floor(st);break;
          case ROUND:return round(st);break;
          case IP:return ip(st);break;
          case FP:return fp(st);break;
          case SIGN:return sign(st);break;
          case ABS:return abs(st);break;
          case MAX:return max(st);break;
          case MIN:return min(st);break;
          case EXP:return exp(st);break;
          case FACT:return fact(st);break;
          case SQRT:return sqrt(st);break;
          case LN:return ln(st);break;
          case LOG:return log(st);break;
          case POW:return pow(st);break;
          case HNL:return hnl(st);break;
          case HNS:return hns(st);break;
          case NHL:return nhl(st);break;
          case NHS:return nhs(st);break;
          case PICK:return pick(st);break;
          case REPEAT:break;
          case DEPTH:return depth(st);break;
          case DROP:return drop(st);break;
          case DROPN:return dropn(st);break;
          case DUP:return dup(st);break;
          case DUPN:return dupn(st);break;
          case ROLL:return roll(st);break;
          case ROLLD:return rolld(st);break;
          case STACK:break;
          case SWAP:return swap(st);break;
          case VARSET:break;
          case MACRO:break;
          case HELP:break;
          case EXIT:break;

    }
    return OK;
}

int execute_operation_times(std::vector<t_var> &st, enum yytokentype toktype)
{
    if(st.size() == 0)
        return NOT_ENOUGH_ARGS;

    t_var val1 = st.back();
    if(val1.second < 0)
        return INVALID_ARG;

    st.pop_back();


    for(uint i = 0; i < val1.second.ToUInt(); i++)
         handle_command_status(execute_operation(st, toktype));

    return OK;
}
