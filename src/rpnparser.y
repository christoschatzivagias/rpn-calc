%{
#include <stdio.h>
#include <string.h>
#include <cstdlib>

#include "operations.h"

extern int yylex(void);
const char *token_name(int t);

void yyerror(const char *str)
{
    fprintf(stderr,"error: %s\n",str);
}

%}

%union
{
    enum yytokentype integer;
    double number;
    char *string;
}
%token-table
%token <number> NUMBER
%token <string> STR
%token <integer> ADD SUB MULT DIV CLVALS CLVARS CL BNOT NOTEQ MOD INC DEC
%token <integer> BWAND BWOR BWXOR BWNOT BWSFTL BWSFTR BAND BOR BXOR LESS LESS_EQ EQ GREATER GREATER_EQ
%token <integer> ACOS ASIN ATAN COS COSH SIN SINH TANH
%token <integer> HEX DECI BIN OCT
%token <integer> CEIL FLOOR ROUND IP FP SIGN ABS MAX MIN EXP FACT SQRT LN LOG POW
%token <integer> HNL HNS NHL NHS
%token <integer> PICK REPEAT DEPTH DROP DROPN DUP DUPN ROLL ROLLD STACK SWAP VARSET MACRO
%token <integer> HELP EXIT SPACE
%type  <integer> operation
%%


input:
    commands
    | stack
    | display_mode
    | macro_decl
    | help
    | exit;


macro_operation:
    NUMBER {
        add_macro_operation(NUM_PUSH, std::to_string($1));
    }
    | operation {
        add_macro_operation(OPERATION, std::to_string($1));
    };

macro_operations:
      | macro_operations macro_operation;


macro_decl:
    MACRO STR macro_operations {
       add_macro($2);
    };


commands:
  | commands command ;

help:
    HELP {
      print_help();
    };

exit:
    EXIT {
        exit(0);
    };

stack:
    STACK {
        set_stack_print_layout(get_stack_print_layout()==Horizontal ? Vertical : Horizontal);
    };

display_mode:
    HEX {
        set_stack_print_display_mode(HEXADEMICAL);
    }
    | DECI {
        set_stack_print_display_mode(DECIMAL);
    }
    | BIN {
        set_stack_print_display_mode(BINARY);
    }
    | OCT {
        set_stack_print_display_mode(OCTA);
    };


operation:
    ADD {
        $$=$1;
    }
    | SUB {
        $$=$1;
    }
    | MULT {
        $$=$1;
    }
    | DIV {
        $$=$1;
    }
    | CLVALS {
        $$=$1;
    }
    | CLVARS {
        $$=$1;
    }
    | CL {
        $$=$1;
    }
    | BNOT {
        $$=$1;
    }
    | NOTEQ {
        $$=$1;
    }
    | MOD {
        $$=$1;
    }
    | INC {
        $$=$1;
    }
    | DEC {
        $$=$1;
    }
    | BWAND {
        $$=$1;
    }
    | BWOR {
        $$=$1;
    }
    | BWXOR {
        $$=$1;
    }
    | BWNOT {
        $$=$1;
    }
    | BWSFTL {
        $$=$1;
    }
    | BWSFTR {
        $$=$1;
    }
    | BAND {
        $$=$1;
    }
    | BOR {
        $$=$1;
    }
    | BXOR {
        $$=$1;
    }
    | LESS {
        $$=$1;
    }
    | LESS_EQ {
        $$=$1;
    }
    | EQ {
        $$=$1;
    }
    | GREATER {
        $$=$1;
    }
    | GREATER_EQ {
        $$=$1;
    }
    | ACOS {
        $$=$1;
    }
    | ASIN {
        $$=$1;
    }
    | ATAN {
        $$=$1;
    }
    | COS {
        $$=$1;
    }
    | COSH {
        $$=$1;
    }
    | SIN {
        $$=$1;
    }
    | SINH {
        $$=$1;
    }
    | TANH {
        $$=$1;
    }
    | CEIL {
        $$=$1;
    }
    | FLOOR {
        $$=$1;
    }
    | ROUND {
        $$=$1;
    }
    | IP {
        $$=$1;
    }
    | FP {
        $$=$1;
    }
    | SIGN {
        $$=$1;
    }
    | ABS {
        $$=$1;
    }
    | MAX {
        $$=$1;
    }
    | MIN {
        $$=$1;
    }
    | EXP {
        $$=$1;
    }
    | FACT {
        $$=$1;
    }
    | SQRT {
        $$=$1;
    }
    | LN {
        $$=$1;
    }
    | LOG {
        $$=$1;
    }
    | POW {
        $$=$1;
    }
    | HNL {
        $$=$1;
    }
    | HNS{
        $$=$1;
    }
    | NHL {
        $$=$1;
    }
    | NHS {
        $$=$1;
    }
    | PICK {
        $$=$1;
    }
    | DEPTH {
        $$=$1;
    }
    | DROP {
        $$=$1;
    }
    | DROPN {
        $$=$1;
    }
    | DUP {
        $$=$1;
    }
    | DUPN {
        $$=$1;
    }
    | ROLL {
        $$=$1;
    }
    | ROLLD {
        $$=$1;
    }
    | SWAP {
        $$=$1;
    };


command:
    NUMBER {
        number(get_stack(), $1);
    }
    |
    SUB NUMBER {
        number(get_stack(), -$2);
    }
    | operation {
        handle_command_status(execute_operation(get_stack(), $1));
    }
    | REPEAT operation {
        handle_command_status(execute_operation_times(get_stack(), $2));
    }
    | STR VARSET {
        handle_command_status(varset(get_stack(), $1));
    }
    | STR
    {
        t_var t = get_variable(get_stack(), $1);
        std::vector<macro_op> v = get_macro($1);

        if(t.first == "" && v.size() == 0)
            yyerror(std::string(std::string("unknown variable or macro ") + $1).c_str());
        else {
            if(t.first != "")
                number(get_stack(), t.second);
            else if(v.size() > 0) {
                std::for_each(v.cbegin(), v.cend(), [](const macro_op &mop){

                    if(mop.first == NUM_PUSH) {
                        number(get_stack(), atof(mop.second.c_str()));
                    } else if(mop.first == OPERATION) {
                        handle_command_status(execute_operation(get_stack(), (enum yytokentype)atoi(mop.second.c_str())));
                    }
                });
            }
        }
    };



%%
const char *token_name(int t) {
  return yytname[YYTRANSLATE(t)];
}
