#include "operations.h"
#include <iostream>
#include <stdlib.h>
#include <string.h>



int main(int argc, char *argv[])
{
    Color::Modifier blue(Color::FG_BLUE);
    Color::Modifier def(Color::FG_DEFAULT);

    if(argc >= 2) {
        if(!strcmp(argv[1],"-h") || !strcmp(argv[1],"--help")) {
            print_help();
            return EXIT_SUCCESS;
        }
    }
    std::string input;
    if(argc == 1) {
        while (true) {
            print_stack(get_stack());
            std::cout << blue << " > " << def;
            getline(std::cin, input);
            yy_scan_string(input.c_str());
            yyparse();
        }
    } else {
        for (int i = 1; i < argc; i++) {
            input = input + std::string(" ") + argv[i];
        }

        yy_scan_string(input.c_str());
        yyparse();
        print_stack(get_stack());
	std::cout << std::endl;

    }
    return EXIT_SUCCESS;
}
