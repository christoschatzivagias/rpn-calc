#!/usr/bin/sh
mkdir build
cd build
cmake ../ -DCMAKE_INSTALL_PREFIX=../install_dir
make
make test
make install
