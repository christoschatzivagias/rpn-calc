#pragma once
#include <string>
#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include <ostream>
#include <ttmath/ttmath.h>
#include "rpnparser.hpp"

namespace Color {
    enum Code {
        FG_RED      = 31,
        FG_GREEN    = 32,
        FG_BLUE     = 34,
        FG_DEFAULT  = 39,
        BG_RED      = 41,
        BG_GREEN    = 42,
        BG_BLUE     = 44,
        BG_DEFAULT  = 49
    };
    class Modifier {
        Code code;
    public:
        Modifier(Code pCode) : code(pCode) {}
        friend std::ostream&
        operator<<(std::ostream& os, const Modifier& mod) {
            return os << "\033[" << mod.code << "m";
        }
    };
}


struct yy_buffer_state;
extern yy_buffer_state *yy_scan_string(const char *yy_str);
extern int yyparse(void);
extern void yyerror(const char *);

enum OperationType {
    NUM_PUSH = 0,
    OPERATION = 1
};

enum CommandStatus{
    OK = 0,
    ERROR = 1,
    NOT_ENOUGH_ARGS = 2,
    INVALID_ARG = 3,
    VARIABLE_EXISTS = 4
};

enum StackPrintLayoutType {
    Horizontal = 0,
    Vertical = 1
};

enum StackPrintDisplayMode {
    DECIMAL = 10,
    HEXADEMICAL = 16,
    BINARY = 2,
    OCTA = 8
};

union FloatUnion {
    float f;
    char  c[sizeof(float)];
    int   i;
};


typedef std::pair<std::string, ttmath::Big<10,20>> t_var;
typedef std::pair<enum OperationType, std::string> macro_op;

void add_macro_operation(enum OperationType otype, const std::string &num);
void handle_command_status(int command_status);
void add_macro(const std::string &name);
std::vector<macro_op> get_macro(const std::string &name);
std::vector<t_var> &get_stack();
enum StackPrintDisplayMode get_stack_display_mode();
void set_stack_print_display_mode(enum StackPrintDisplayMode value);
enum StackPrintLayoutType get_stack_print_layout();
void set_stack_print_layout(enum StackPrintLayoutType value);
void print_help();

void print_hexa(float num);
void print_octa(float num);
void print_bin(float num);
void print_stack(std::vector<t_var> s);
t_var get_variable(const std::vector<t_var> &st, const std::string &name);

bool check_double_integer(double val);
bool replace(std::string& str, const std::string& from, const std::string& to);
std::vector<std::string> split(const std::string &s);

int number(std::vector<t_var> &st, ttmath::Big<10, 20> num);
int add(std::vector<t_var> &st);
int subtract(std::vector<t_var> &st);
int multiply(std::vector<t_var> &st);
int divide(std::vector<t_var> &st);
int clearvals(std::vector<t_var> &st);
int clearvars(std::vector<t_var> &st);
int clear(std::vector<t_var> &st);
int booleannot(std::vector<t_var> &st);
int notequalto(std::vector<t_var> &st);
int modulus(std::vector<t_var> &st);
int increment(std::vector<t_var> &st);
int decrement(std::vector<t_var> &st);

int bitwand(std::vector<t_var> &st);
int bitwor(std::vector<t_var> &st);
int bitwxor(std::vector<t_var> &st);
int bitwnot(std::vector<t_var> &st);
int bitwshiftleft(std::vector<t_var> &st);
int bitwshiftright(std::vector<t_var> &st);

int band(std::vector<t_var> &st);
int bor(std::vector<t_var> &st);
int bxor(std::vector<t_var> &st);

int lessthan(std::vector<t_var> &st);
int lessthanorequal(std::vector<t_var> &st);
int equal(std::vector<t_var> &st);
int greaterthan(std::vector<t_var> &st);
int greaterthanorequal(std::vector<t_var> &st);

int acos(std::vector<t_var> &st);
int asin(std::vector<t_var> &st);
int atan(std::vector<t_var> &st);
int cos(std::vector<t_var> &st);
int cosh(std::vector<t_var> &st);
int sin(std::vector<t_var> &st);
int sinh(std::vector<t_var> &st);
int tanh(std::vector<t_var> &st);

int ceil(std::vector<t_var> &st);
int floor(std::vector<t_var> &st);
int round(std::vector<t_var> &st);
int ip(std::vector<t_var> &st);
int fp(std::vector<t_var> &st);
int sign(std::vector<t_var> &st);
int abs(std::vector<t_var> &st);
int max(std::vector<t_var> &st);
int min(std::vector<t_var> &st);


int exp(std::vector<t_var> &st);
int fact(std::vector<t_var> &st);
int sqrt(std::vector<t_var> &st);
int ln(std::vector<t_var> &st);
int log(std::vector<t_var> &st);
int pow(std::vector<t_var> &st);


int hnl(std::vector<t_var> &st);
int hns(std::vector<t_var> &st);
int nhl(std::vector<t_var> &st);
int nhs(std::vector<t_var> &st);

int pick(std::vector<t_var> &st);
int repeat(std::vector<t_var> &st);
int depth(std::vector<t_var> &st);
int drop(std::vector<t_var> &st);
int dropn(std::vector<t_var> &st);
int dup(std::vector<t_var> &st);
int dupn(std::vector<t_var> &st);
int roll(std::vector<t_var> &st);
int rolld(std::vector<t_var> &st);
int vector(std::vector<t_var> &st);
int swap(std::vector<t_var> &st);
int varset(std::vector<t_var> &st, const std::string &varname);

int help();
int exit();

std::vector<std::string> split(const std::string &s, char delim);
int execute_operation(std::vector<t_var> &st, enum yytokentype toktype);
int execute_operation_times(std::vector<t_var> &st, enum yytokentype toktype);
